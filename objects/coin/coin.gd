extends Area2D


signal got_coin
signal end_coin


var pos_state: int = 0
var destroying: bool = false
export var respawn_delay: float = 15.0


func _ready():
	$AnimationPlayer.play("IDLE")
	
	var _t = self.connect("got_coin", get_parent().get_parent().get_node("CanvasLayer/GUI"), "_on_got_coin")
	_t = self.connect("end_coin", get_parent().get_parent().get_node("CanvasLayer/GUI"), "_on_end_coin")



func collect():
	Global.current_score += 150
	destroying = true
	
	emit_signal("got_coin")
	
	$AnimationPlayer.play("FLASH")
	$"1up".play(0.0)



func _on_coin_body_entered(body):
	
	if body.is_in_group("player_node") and !destroying:
		collect()


func _on_1up_finished():
	$Sprite.hide()
	emit_signal("end_coin")
	$respawn.start(respawn_delay)


func _on_respawn_timeout():
	
#	pos_state += 1
#
#	if pos_state > 1:
#		pos_state = -1
#
#	self.global_position.x += 32*p
#
#	if pos_state == 0:
#		self.global_position.x += 32
	
	
	$AnimationPlayer.play("IDLE")
	$Sprite.show()
	destroying = false

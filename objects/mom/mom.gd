extends KinematicBody2D




onready var sneeze_scene: PackedScene = preload("res://objects/sneeze/sneeze.tscn")
onready var path_node: Node = get_parent().get_node("PathFinder")
onready var animator: AnimationPlayer = $AnimationPlayer
onready var spr: Sprite = $Sprite
#onready var positions_folder: Node2D = get_parent().get_node("mom_positions")
onready var sneeze_timer_node: Timer = $sneeze_timer
onready var sneeze_sound: AudioStreamPlayer2D = $sneeze_audio
onready var exclamation_sprite: Sprite = $exclamation_mark


onready var map_points: Array = [
	get_parent().get_node("mom_positions/0").global_position,
	get_parent().get_node("mom_positions/1").global_position,
	get_parent().get_node("mom_positions/2").global_position,
	get_parent().get_node("mom_positions/3").global_position,
	get_parent().get_node("mom_positions/4").global_position
	]


var point_index: int = 0


var rng := RandomNumberGenerator.new()
var not_moving: bool = false
var velocity: Vector2 = Vector2.ZERO
var path: PoolVector2Array
var speed = 60


func _ready():
	
	point_index = rng.randi_range(0, map_points.size()-1)

func _physics_process(delta):
	
	
	
	path = path_node.pathfind(self.global_position, map_points[point_index])
	
	if path.size() > 0:
		move_along_path(speed*delta*Global.speed_multiplier)
		
	else:
		not_moving = false
	
	if not_moving:
		animator.play("IDLE")
		#animator.play("WALK")
	else:
		#animator.play("IDLE")
		animator.play("WALK")



func move_along_path(distance: float) -> void:
	
	#var start_point := self.position
	var start_point := self.global_position
	
	for _i in range(path.size()):
		
		var distance_to_next_point := start_point.distance_to(path[0])
		
		if distance_to_next_point < 5:
			not_moving = true
		else:
			not_moving = false
		
		if path[0].x > self.global_position.x:
			spr.flip_h = false
		else:
			spr.flip_h = true
		
		
		if distance <= distance_to_next_point and distance >= 0.0:
			position = start_point.linear_interpolate(path[0], distance / distance_to_next_point)
			break
		elif distance < 0.0:
			position = path[0]
			break
		
		distance -= distance_to_next_point
		start_point = path[0]
		path.remove(0)




func sneeze():
	
	var new_instance: Area2D = sneeze_scene.instance()
	
	new_instance.scale = Vector2(0.89, 0.89)
	
	
	exclamation_sprite.show()
	yield(get_tree().create_timer(1), "timeout")
	exclamation_sprite.hide()
	
	
	yield(get_tree().create_timer(0.25), "timeout")
	#$gasp_audio.play(0.0)
	add_child(new_instance)
	
	
	yield(get_tree().create_timer(0.2), "timeout")
	
	sneeze_sound.play(0.0)
	
	print("mom Sneeze")


func _on_sneeze_timer_timeout():
	sneeze()


func _on_change_pos_timeout():
	rng.randomize()
	point_index = rng.randi_range(0, map_points.size()-1)
	$change_pos.start(rng.randf_range(14, 18))

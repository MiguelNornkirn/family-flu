extends Control


func _process(_delta):
	
	if Global.current_score < 1000: 
		$score.text = "Score: " + str(Global.current_score) # normal
	elif Global.current_score < 1000000:
		$score.text = "Score: " + str(stepify(Global.current_score/1000.0, 0.01)) + str("K")
	else:
		$score.text = "Score: " + str(stepify(Global.current_score/1000000.0, 0.01)) + str("M")
	
	$fps.text = "FPS: " + str(Engine.get_frames_per_second())
	
	if Global.is_in_danger:
		$flash.play("FLASH")
	else:
		$flash.play("IDLE")


# signals
func _on_got_coin():
	$points_flash.play("FLASH")

func _on_end_coin():
	$points_flash.play("DEFAULT")

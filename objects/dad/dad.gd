extends StaticBody2D

onready var sneeze_scene: PackedScene = preload("res://objects/sneeze/sneeze.tscn")
onready var sneeze_sound: AudioStreamPlayer2D = $sneeze_sound
onready var exclamation_sprite: Sprite = $exclamation
onready var sneeze_timer_node: Timer = $sneeze_timer
var rng := RandomNumberGenerator.new()




func _on_sneeze_timer_timeout():
	
	var new_instance: Area2D = sneeze_scene.instance()
	
	rng.randomize()
	
	new_instance.scale = Vector2(2.5, 2.5)
	
	var _time = Global.DAD_SNEEZE_INTERVAL-rng.randf_range(0.0, 10.0)
	
	if _time < 10:
		_time = 10
	
	exclamation_sprite.show()
	yield(get_tree().create_timer(1), "timeout")
	exclamation_sprite.hide()
	
	add_child(new_instance)
	
	yield(get_tree().create_timer(0.1), "timeout")
	sneeze_sound.play(0.0)
	
	print("Dad sneeze")
	sneeze_timer_node.start(_time)

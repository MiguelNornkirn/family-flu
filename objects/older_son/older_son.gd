extends KinematicBody2D




onready var sneeze_scene: PackedScene = preload("res://objects/sneeze/sneeze.tscn")
onready var path_node: Node = get_parent().get_node("PathFinder")
onready var animator_node: AnimationPlayer = $AnimationPlayer
onready var sprite_node: Sprite = $Sprite
onready var player_node: KinematicBody2D = get_parent().get_node("player")
onready var sneeze_timer_node: Timer = $sneeze 
onready var sneeze_sound: AudioStreamPlayer2D = $sneeze_audio
onready var exclamation_sprite: Sprite = $exclamation


var dir_vector: Vector2 = Vector2.ZERO
var velocity: Vector2 = Vector2.ZERO
var seeking: bool = false
var not_moving: bool = true
var path: PoolVector2Array
var speed = 75
var is_on_screen: bool = false
var rng := RandomNumberGenerator.new()



func _physics_process(delta):
	
	
	dir_vector = Vector2.ZERO
	
	if seeking and (self.global_position.distance_to(player_node.global_position) < 150):
		path = path_node.pathfind(self.global_position, player_node.global_position-Vector2(20, 20))
	elif seeking:
		path = path_node.pathfind(self.global_position, player_node.global_position)
	
	if path.size() > 0 and seeking:
		move_along_path(speed*delta*Global.speed_multiplier)
	else:
		not_moving = true
	
	if not_moving:
		animator_node.play("IDLE")
		
		if player_node.global_position.x > self.global_position.x:
			sprite_node.flip_h = false
		else:
			sprite_node.flip_h = true
		
	else:
		animator_node.play("WALK")
	
	if !seeking:
		start_seeking()


func move_along_path(distance: float) -> void:
	
	var start_point := self.position
	
	for _i in range(path.size()):
		
		var distance_to_next_point := start_point.distance_to(path[0])
		
		if distance_to_next_point < 5:
			not_moving = true
		else:
			not_moving = false
		
		if path[0].x > self.global_position.x:
			sprite_node.flip_h = false
		else:
			sprite_node.flip_h = true
		
		if distance <= distance_to_next_point and distance >= 0.0:
			position = start_point.linear_interpolate(path[0], distance / distance_to_next_point)
			break
		elif distance < 0.0:
			position = path[0]
			break
		
		distance -= distance_to_next_point
		start_point = path[0]
		path.remove(0)


func sneeze():
	
	var new_instance: Area2D = sneeze_scene.instance()
	
	rng.randomize()
	
	new_instance.scale = Vector2(0.95, 0.95)
	
	var _time = Global.OLDER_SON_SNEEZE_INTERVAL-rng.randf_range(0.0, 3.0)*Global.sneeze_multiplier
	
	if _time < 2:
		_time = 2
	
	exclamation_sprite.show()
	yield(get_tree().create_timer(1), "timeout")
	exclamation_sprite.hide()
	
	
	yield(get_tree().create_timer(0.25), "timeout")
	$gasp_audio.play(0.0)
	add_child(new_instance)
	
	
	yield(get_tree().create_timer(0.2), "timeout")
	
	sneeze_sound.play(0.0)
	
	print(_time)
	print(Global.sneeze_multiplier)
	sneeze_timer_node.start(_time)


func _on_notifier_screen_entered():
	is_on_screen = true
	
	yield(get_tree().create_timer(1.0), "timeout")
	
	if is_on_screen:
		seeking = true


func _on_notifier_screen_exited():
	is_on_screen = false
	
	yield(get_tree().create_timer(1.5), "timeout")
	if !is_on_screen:
		seeking = false


func start_seeking():
	rng.randomize()
	yield(get_tree().create_timer(2-rng.randf_range(0.0, 0.6)), "timeout")
	seeking = true


func _on_sneeze_timeout():
	sneeze()

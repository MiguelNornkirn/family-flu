extends Node

onready var game_over_scene: PackedScene = preload("res://scenes/GameOver.tscn")


func _state_ready():
	
	get_parent().get_parent().get_node("spr").hide()
	get_parent().get_parent().get_node("dead").show()
	
	
	yield(get_tree().create_timer(1.0), "timeout")
	get_parent().get_parent().velocity = Vector2.ZERO
	
	
	yield(get_tree().create_timer(2.0), "timeout")
	
	if Global.current_score > Global.high_score:
		Global.high_score = Global.current_score # "user://savegame.save"
		SaveAndLoad.save_data("user://score.json", Global.current_score, true)
	
	print("Game Over!")
	
	get_tree().paused = false
	get_parent().get_parent().get_parent().get_node("CanvasLayer/pause").can_pause = false
	var _t = get_tree().change_scene_to(game_over_scene)


func _state_process(_delta):
	pass

extends Node

onready var player_node = get_parent().get_parent()

var can_walk: bool = false




func _state_ready():
	
	
	
	can_walk = false
	allow_walk()


func _state_process(_delta: float):
	
	player_node.velocity = Vector2(0, 0)
	
	
	if is_moving(player_node.is_on_wall()) and can_walk:
		move()


func is_moving(wall: bool) -> bool:
	
	
	var input_vector: Vector2 = Vector2.ZERO
	
	if wall and (Input.is_action_just_pressed("mDown") or Input.is_action_just_pressed("mUp") or Input.is_action_just_pressed("mRight") or Input.is_action_just_pressed("mLeft")):
		return true
	elif !wall:
		input_vector.x = Input.get_action_strength("mRight")-Input.get_action_strength("mLeft")
		input_vector.y = Input.get_action_strength("mDown")-Input.get_action_strength("mLeft")
	

	
	
	if input_vector.x != 0 or input_vector.y != 0:
		return true
	
	return false


func move():
	
	# delay before moving
	yield(get_tree().create_timer(0.009), "timeout") # 0.015
	
	player_node.switch_state("WALK")


func allow_walk():
	
	#yield(get_tree().create_timer(0.05), "timeout")
	can_walk = true

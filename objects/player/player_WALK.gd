extends Node

const SPEED = 175

onready var player_node = get_parent().get_parent()

var input_vector: Vector2 = Vector2.ZERO
var dcc := 8



func _state_ready() -> void:
	pass


func _state_process(delta: float) -> void:
	
	get_dir()
	
	if input_vector != Vector2.ZERO:
		player_node.velocity = input_vector*SPEED
	else:
		player_node.velocity = lerp(player_node.velocity, Vector2.ZERO, dcc*delta)
	
	if int(player_node.velocity.x) == 0 and int(player_node.velocity.y) == 0:
		player_node.switch_state("IDLE")
	



func get_dir() -> void:
	
	input_vector = Vector2.ZERO
	
	input_vector.x = Input.get_action_strength("mRight")-Input.get_action_strength("mLeft")
	input_vector.y = Input.get_action_strength("mDown")-Input.get_action_strength("mUp")
	
	input_vector = input_vector.normalized()

extends KinematicBody2D


onready var animator_node: AnimationPlayer = $animator
onready var sprite_node: Sprite = $spr


onready var _player_states: Dictionary = {
	"IDLE": $STATES/IDLE,
	"WALK": $STATES/WALK,
	"DEATH": $STATES/DEATH
}

var _current_state: String = "IDLE"

var velocity = Vector2.ZERO
var e_speed_modifier = 1
var facing_left: bool = false
var close_people: int = 0 # pessoas próximas
var rng := RandomNumberGenerator.new()

func _ready() -> void:
	
	switch_state("IDLE")


func _physics_process(delta) -> void:
	
	
	_player_states[_current_state]._state_process(delta)
	
	velocity = move_and_slide(velocity)*e_speed_modifier
	
	if Global.IS_DEBUGGING:
		_update_debug()
	
	if int(velocity.x) > 0:
		sprite_node.flip_h = false
		facing_left = false
		
	elif int(velocity.x) < 0:
		sprite_node.flip_h = true
		facing_left = true
	
	
	sprite_node.flip_h = facing_left
	
	if close_people > 0:
		Global.is_in_danger = true
	else:
		Global.is_in_danger = false



func _update_debug() -> void:
	
	$debug_text.show()
	
	$debug_text/fps.text = "FPS: " + str(Engine.get_frames_per_second())
	$debug_text/state.text = "Current State: " + _current_state
	$debug_text/speed.text = "Velocity: " + str(velocity)
	$debug_text/area.text = "SPIA: 0" + str(close_people) 


func switch_state(state_to_switch: String) -> void:
	
	_current_state = state_to_switch
	
	if _current_state != "DEATH":
		animator_node.play(_current_state)
	
	_player_states[_current_state]._state_ready()


func _on_hitbox_area_entered(area: Area2D):
	
	if area.is_in_group("sneeze"):
		yield(get_tree().create_timer(0.3), "timeout")
		switch_state("DEATH")
		return
	
	if area.is_in_group("family"):
		close_people += 1



func _on_hitbox_area_exited(area: Area2D):
	
	if area.is_in_group("family"):
		close_people -= 1


func _on_close_timer_timeout():
	
	rng.randomize()
	
	var n = rng.randi_range(1, 20)
	
	if n*close_people > 17:
		switch_state("DEATH")


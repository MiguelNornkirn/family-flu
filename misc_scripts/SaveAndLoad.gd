"""
		  SSAVE AND LOAD - A simple GDScript library to make writting saving
		systems for Godot easier.


~ Code written by SoicBR - 2020


No credit needed. But i would be very happy if you do it

"""

extends Node



func save_data(path: String, data_to_save, needs_enc: bool) -> void: # Saves data in .json files
	
	var file
	
	file = File.new()
	
	
	if needs_enc:
		file.open_encrypted_with_pass(path, File.WRITE, OS.get_unique_id())
	else:
		file.open(path, File.WRITE)
	
	file.store_string(to_json(data_to_save))
	
	file.close()


func load_data(data_path: String, needs_enc: bool): # Loads .json files
	
	var file = File.new()
	
	if !file.file_exists(data_path):
		push_error("[SSave and Load] The save file '" + str(data_path) + "' don't exists")
		return
	
	
	if needs_enc:
		file.open_encrypted_with_pass(data_path, File.READ, OS.get_unique_id())
	else:
		file.open(data_path, File.READ)
	
	var text = file.get_as_text()
	
	var data = parse_json(text)
	
	file.close()
	
	return data


func exist_check(data_path: String) -> bool: # Checks if a file exists
	
	var file = File.new()
	
	if file.file_exists(data_path):
		file.close()
		return true
	
	return false


func delete_data(data_to_delete: String): # Delete files
	
	var dir = Directory.new()
	dir.remove(data_to_delete)

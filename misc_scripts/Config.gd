extends Node


var post_processing: bool = true
var show_fps: bool = false


func _ready(): # load save data
	
	if SaveAndLoad.exist_check("user://pp.json"):
		post_processing = bool(SaveAndLoad.load_data("user://pp.json", false))
	else:
		print("No PP save data found")
	
	if SaveAndLoad.exist_check("user://fps.json"):
		show_fps = bool(SaveAndLoad.load_data("user://fps.json", false))
	else:
		print("No FPS save data found")


func save_settings() -> void:
	
	SaveAndLoad.save_data("user://pp.json", post_processing, false)
	SaveAndLoad.save_data("user://fps.json", show_fps, false)

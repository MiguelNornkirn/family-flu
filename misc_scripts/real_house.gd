extends Node2D


func _ready():
	
	var rng := RandomNumberGenerator.new()
	
	yield(get_tree().create_timer(rng.randi_range(4, 10)), "timeout")
	get_node("older_son").seeking = true
	
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


func _on_incremente_hs_timeout():
	
	Global.current_score += 1
	Global.sneeze_multiplier += 0.15
	
	if Global.sneeze_multiplier < 0:
		Global.sneeze_multiplier = 0.0001

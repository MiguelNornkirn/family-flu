extends Control


var can_pause: bool = true


func _process(_delta):
	
	if Input.is_action_just_pressed("pause") and can_pause:
		pause_game()
	
	if !can_pause:
		get_tree().paused = false
		self.set_physics_process(false)


func pause_game() -> void:
	
	yield(get_tree().create_timer(0.15), "timeout")
	get_tree().paused = !get_tree().paused
	
	if get_tree().paused:
		self.visible = true
		get_parent().get_node("GUI").visible = false
	else:
		self.visible = false
		get_parent().get_node("GUI").visible = true
	
	$bleep.play(0.0)

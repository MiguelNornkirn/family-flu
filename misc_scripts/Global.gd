extends Node


const IS_DEBUGGING = false



var speed_multiplier: float = 1.00 # aumenta com o tempo
var sneeze_multiplier: float = 1.00 # vai diminuindo com o tempo

# seconds
var current_score: int = 0
var high_score: int = 0

var is_in_danger: bool = false

const OLDER_SON_SNEEZE_INTERVAL = 15.00 
#const MOM_SNEEZE_INTERVAL = 25.00 
const DAD_SNEEZE_INTERVAL = 30.00


func _ready():
	# load saves
	
	if SaveAndLoad.exist_check("user://score.json"):
		high_score = int(SaveAndLoad.load_data("user://score.json", true))
		print("Loaded file, current highscore is " + str(high_score))
	else:
		print("No save file exists")

extends Node


onready var nav_node: Navigation2D = get_parent().get_node("path")



func pathfind(from_point: Vector2, to_point: Vector2) -> PoolVector2Array:
	
	var new_path := nav_node.get_simple_path(from_point, to_point, false)
	
	return new_path

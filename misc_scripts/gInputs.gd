"""

by SoicBR

Aqui existem inputs que são usados em todo o jogo, sem exceções.

"""
extends Node


func _physics_process(_delta):
	
	if Global.IS_DEBUGGING:
		debug_binds()
	
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen


func debug_binds() -> void:
	
	if Input.is_action_just_pressed("debug_restart"):
		# temp variable to stop warnings from happening
		var _t = get_tree().reload_current_scene()

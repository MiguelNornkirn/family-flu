extends CanvasLayer


func _ready():
	
	if Config.show_fps:
		$GUI/fps.show()
	
	if !Config.post_processing:
		$PP.queue_free()

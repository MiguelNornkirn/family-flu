Source code from an old game jam game of mine. I think it was my second
completed game.

The code here does not represent my current coding skills.

Download the game [here](https://soicbr-games.itch.io/family-flu).

# Family flu

Your whole family is sick! Getting sick is horrible, so the only logical option
is to avoid them at all costs!

## Featuring

### Your dad

He sits on the sofa and spends the entire day watching TV. His sneezes are
powerful, a virus' best friend.

### Your mom

She walks around the house doing things, a cold ain't stopping her.

### Your younger brother

A little kid who really wants to play with you! He is sick, though.

## Theme

This game was made in a few the for the Godot Wild Jam 24, the theme was
"family".

# "Why is this repo so big?" 

I didn't know what a .gitignore was at the time, so the repo used to include
builds. 

# "Why does the project mix English and Portuguese?"

I have no idea.

# Licensing

All of the game's *original* code and art are available under the GPL3 license,
which means you are free to modify and distribute the game to whoever you want,
but you must distribute the source code and make your modifications GPL
licensed too.

This project uses code and art from third-parties, see
[CREDITS.md](CREDITS.md). Note that these are NOT necessarily under the GPL.

# Screenshots

![Screenshot](screenshots/0.png)
![Screenshot](screenshots/1.png)
![Screenshot](screenshots/2.png)
![Screenshot](screenshots/3.png)
![Screenshot](screenshots/4.png)
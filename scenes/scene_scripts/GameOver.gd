extends Control

const HS_MESSAGE = "Your high score: "
const CS_MESSAGE = "Your current score: "


func _ready():
	
	if Global.high_score < 1000: 
		$labels/high_score.text = HS_MESSAGE + str(Global.high_score) # normal
	elif Global.high_score < 1000000:
		$labels/high_score.text = HS_MESSAGE + str(stepify(Global.high_score/1000.0, 0.01)) + str("K")
	else:
		$labels/high_score.text = HS_MESSAGE + str(stepify(Global.high_score/1000000.0, 0.01)) + str("M")
	
	if Global.current_score < 1000: 
		$labels/current_score.text = CS_MESSAGE + str(Global.current_score) # normal
	elif Global.current_score < 1000000:
		$labels/current_score.text  = CS_MESSAGE + str(stepify(Global.current_score/1000.0, 0.01)) + str("K")
	else:
		$labels/current_score.text  = CS_MESSAGE + str(stepify(Global.current_score/1000000.0, 0.01)) + str("M")
	
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_try_again_button_up():
	
	reset()
	var _t = get_tree().change_scene("res://scenes/house.tscn")


func _on_menu_button_up():
	
	reset()
	var _t = get_tree().change_scene("res://scenes/MainMenu.tscn")


func reset() -> void:
	Global.is_in_danger = false
	Global.current_score = 0
	Global.sneeze_multiplier = 1.00

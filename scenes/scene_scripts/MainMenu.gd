extends Node2D



onready var layouts: Array = [
	preload("res://scenes/menu/00.tscn"), # main
	preload("res://scenes/menu/01.tscn"), # about
	preload("res://scenes/menu/02.tscn") # settings
]


func _ready():
	
	change_layout(layouts[0])


func change_layout(layout: PackedScene) -> void:
	
	var new = layout.instance()
	self.add_child(new)


func _on_start_button_up():
	
	var _t = get_tree().change_scene("res://scenes/house.tscn")


func _on_about_button_up():
	
	$"00".queue_free()
	change_layout(layouts[1])


func _on_settings_button_up():
	
	$"00".queue_free()
	change_layout(layouts[2])


func _on_back_button_up():
	
	$"01".queue_free()
	change_layout(layouts[0])


func _on_s_back_button_up():
	
	$"02".queue_free()
	change_layout(layouts[0])

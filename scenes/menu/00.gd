extends Node2D


func _ready():
	$start.connect("button_up", get_parent(), "_on_start_button_up")
	$about.connect("button_up", get_parent(), "_on_about_button_up")
	$settings.connect("button_up", get_parent(), "_on_settings_button_up")

extends Node2D


func _ready():
	
	$enable_pp.pressed = Config.post_processing
	$enable_fps.pressed = Config.show_fps
	
	$s_back.connect("button_up", get_parent(), "_on_s_back_button_up")

func _on_enable_pp_toggled(button_pressed):
	
	Config.post_processing = button_pressed
	
	Config.save_settings()


func _on_enable_fps_toggled(button_pressed):
	
	Config.show_fps = button_pressed
	
	Config.save_settings()

